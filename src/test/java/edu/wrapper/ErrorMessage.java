package edu.wrapper;

        import edu.driver.DriverSingleton;
        import org.openqa.selenium.By;
        import org.openqa.selenium.WebDriver;

public class ErrorMessage {
    private WebDriver driver = DriverSingleton.getInstance();
    private By locator;

    public ErrorMessage(By locator){
        this.locator=locator;
    }
    public String getText(){
        return driver.findElement(locator).getText();
    }
}
