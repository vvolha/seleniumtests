package edu.wrapper;

import edu.driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Button {
    private WebDriver driver = DriverSingleton.getInstance();
    private By locator;

    public Button(By locator) {
        this.locator = locator;
    }

    public void click() {
        Wait<WebDriver> wait = new WebDriverWait(driver, 8);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
    }

    public String getText() {
        return driver.findElement(locator).getText();
    }

    public boolean isClickable() {
        return driver.findElement(locator).isEnabled();
    }


    public void place() {
        driver.findElement(locator);
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(locator));
        action.perform();
    }

    public boolean isSelected() {
        return driver.findElement(locator).isSelected();
    }
}
