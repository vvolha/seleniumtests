package edu.wrapper;

import edu.driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Checkbox {
    private WebDriver driver = DriverSingleton.getInstance();
    private By locator;
    public Checkbox(By locator){
        this.locator=locator;
    }
    public void check(){
        Wait<WebDriver> wait = new WebDriverWait(driver,8);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
    }
    public boolean isClickable(){
        return driver.findElement(locator).isEnabled();
    }
    public boolean isSelected(){
        return driver.findElement(locator).isSelected();
    }

}

