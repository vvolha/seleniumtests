package edu.wrapper;

import edu.driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Card {

    private static final By TITLE = By.xpath(".//div[contains(@class, 'title')]");
    private static final By BLOCK_ATTRIBUTE = By.xpath(".//div[contains(@class, 'block-attribute')]");
    private static final By LEVEL = By.xpath(".//div[contains(@class, 'point-donut')]");
    private static final By NUMBER_OF_EMPLOYEES = By.xpath(".//div[contains(@class, 'block-icon font-normal-24')]");
    private static final By DIVISION = By.xpath(".//div[contains(@class, 'tc-orgpotential_card-division')]");

    private WebDriver driver = DriverSingleton.getInstance();
    private By locator;
    public Card(By locator){
        this.locator=locator;
    }
    public void click(){
        Wait<WebDriver> wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        driver.findElement(locator).click();
    }
    public void place(){
        driver.findElement(locator);
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(locator));
        action.perform();
    }

    public String getTitle(){
        return driver.findElement(locator).findElement(TITLE).getText();
    }
    public String getLevel() {
        return driver.findElement(locator).findElement(LEVEL).getText();
    }

    public String getNumberOfEmployees() {
        return driver.findElement(locator).findElement(NUMBER_OF_EMPLOYEES).getText();
    }

    public boolean isClickable(){
        return driver.findElement(locator).isEnabled();
    }


}
