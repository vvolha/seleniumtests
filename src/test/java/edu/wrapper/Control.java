package edu.wrapper;

import edu.driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Control {

    public static final Button DOWNLOAD =new Button(By.xpath("//body[@class='yui3-skin-sam controls-visible guest-site signed-in public-page site']"));
    private WebDriver driver = DriverSingleton.getInstance();
    private By locator;
    public Control(By locator) {
        this.locator = locator;
    }
    public void waitDownloading() {
        Wait<WebDriver> wait = new WebDriverWait(driver, 8);
        wait.until(ExpectedConditions.elementToBeSelected(locator));
    }
}
