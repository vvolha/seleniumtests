package edu.test;

import edu.driver.DriverSingleton;
import edu.pageObject.Jobprofile;

import edu.pageObject.HrPage;
import edu.pageObject.LoginPage;
import edu.wrapper.Card;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static edu.pageObject.HrPage.ADD_COMPETENCY;
import static org.assertj.core.api.Assertions.assertThat;

public class ExampleTest {

    WebDriver driver;
    private HrPage HrPage;
    private Jobprofile Jobprofile;
    private LoginPage loginPage;
   // private OrgPotentialPage orgPotentialPage;

    @BeforeEach
    public void setUp() {
        driver = DriverSingleton.getInstance();
        loginPage = new LoginPage(driver);
        HrPage = new HrPage(driver);
        Jobprofile = new Jobprofile(driver);
    }

    @Test
    public void enter() {
        // given
        String expectedTitle = "Главная - Конструктор Талантов";
        // when
        driver.get("http://tc-stage05.k8s.iba/");
        WebElement searchField = driver.findElement(By.name("_58_login"));
        searchField.sendKeys("zaitsev@tc.by");
        searchField.sendKeys(Keys.ENTER);
        WebElement searchPasswordField = driver.findElement(By.name("_58_password"));
        searchPasswordField.sendKeys("welcome");
        searchPasswordField.sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//div[@class='button-holder ']")).click();
        String actualTitle = driver.getTitle();
        // then
        assertThat(actualTitle).isEqualTo(expectedTitle);
    }

    @Test
    public void login() {
        // given
        String expectedTitle = "Не удалось войти в систему.\n" +
                "Email или пароль введены неверно. Пожалуйста, попробуйте еще раз.";
        // when
        driver.get("http://tc-stage05.k8s.iba/");
        WebElement searchField = driver.findElement(By.name("_58_login"));
        searchField.sendKeys("111");
        WebElement searchPasswordField = driver.findElement(By.name("_58_password"));
        searchPasswordField.sendKeys("welcome");
        searchPasswordField.sendKeys(Keys.ENTER);

        searchField.sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//div[@class='button-holder ']")).click();

        String actualTitle = driver.findElement(By.xpath("//div[@class='login-error']")).getText();

        // then
        assertThat(actualTitle).isEqualTo(expectedTitle);
    }

    @Test
    public void password() {
        // given
        String expectedTitle = "Не удалось войти в систему.\n" +
                "Email или пароль введены неверно. Пожалуйста, попробуйте еще раз.";
        // when
        driver.get("http://tc-stage05.k8s.iba/");
        WebElement searchField = driver.findElement(By.name("_58_login"));
        searchField.sendKeys("zaitsev@tc.by");
        WebElement searchPasswordField = driver.findElement(By.name("_58_password"));
        searchPasswordField.sendKeys("111");
        searchPasswordField.sendKeys(Keys.ENTER);

        searchField.sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//div[@class='button-holder ']")).click();

        String actualTitle = driver.findElement(By.xpath("//div[@class='login-error']")).getText();

        // then
        assertThat(actualTitle).isEqualTo(expectedTitle);
    }

    @Test
    public void example() {
        // given
        String expectedTitle = "IBA BY";
        // when
        driver.get("https://www.google.com/");
        WebElement searchField = driver.findElement(By.name("q"));
        searchField.sendKeys("IBA");
        searchField.sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//div[@class='rc']//a[1]")).click();


        String actualTitle = driver.getTitle();
        // then
        assertThat(actualTitle).isEqualTo(expectedTitle);
    }
    @Test
    public void loginWithPageObject() {
        driver.get("http://tc-stage05.k8s.iba/");
        loginPage.login("zaitsev", "welcome");
        assertThat(loginPage.errorMessage()).isEqualTo("Не удалось войти в систему.\n" +
                "Email или пароль введены неверно. Пожалуйста, попробуйте еще раз.");
    }

    @Test
    public void hrWithPageObject() {
        // given
        driver.get("http://tc-stage05.k8s.iba/");
        loginPage.login("zaitsev@tc.by", "welcome");
        driver.get("http://tc-stage05.k8s.iba/web/guest/strategy/org-potential");
        // when
        String actualTitle = driver.getTitle();
        // then
        assertThat(actualTitle).isEqualTo("Организационный потенциал - Конструктор Талантов");
    }

    @Test
    public void addFirstCompetency() {
        // given

        driver.get("http://tc-stage05.k8s.iba/");
        loginPage.login("zaitsev@tc.by", "welcome");
        driver.get("http://tc-stage05.k8s.iba/web/guest/strategy/org-potential");
        // when
        ADD_COMPETENCY.click();
        HrPage.pushButtonAdd();
        Card card = HrPage.getOrgPotentialCardElementByNumber(3);
        // then
        assertThat(card.getTitle()).isEqualTo("JavaScript");
        assertThat(card.getNumberOfEmployees()).isEqualTo("0");
        assertThat(card.getLevel()).isEqualTo("0%");
    }

    @Test
    public void addEmptyCompetency() {
        // given
        driver.get("http://tc-stage05.k8s.iba/");
        loginPage.login("zaitsev@tc.by", "welcome");
        driver.get("http://tc-stage05.k8s.iba/web/guest/strategy/org-potential");
        // when
        ADD_COMPETENCY.click();
        HrPage.pushButtonAdd();
        String actualTitle = driver.getTitle();
        // then
        assertThat(actualTitle).isEqualTo("Организационный потенциал - Конструктор Талантов");
    }

    @Test
    public void addCertainCompetency() {
        // given

        driver.get("http://tc-stage05.k8s.iba/");
        loginPage.login("zaitsev@tc.by", "welcome");
        driver.get("http://tc-stage05.k8s.iba/web/guest/strategy/org-potential");
        String competency="JavaScript";
        // when
        HrPage.addCompetency(competency);
        HrPage.pushButtonAdd();
        Card card = HrPage.getOrgPotentialCardElementByNumber(3);
        // then
        assertThat(card.getTitle()).isEqualTo("JavaScript");
        assertThat(card.getNumberOfEmployees()).isEqualTo("0");
        assertThat(card.getLevel()).isEqualTo("0%");
    }


    @Test
    public void orgPageTest() {
        driver.get("http://tc-stage05.k8s.iba/");
        loginPage.login("zaitsev@tc.by", "welcome");
        driver.get("http://tc-stage05.k8s.iba/web/guest/strategy/org-potential");

        Card card = HrPage.getOrgPotentialCardElementByNumber(3);

        assertThat(card.getTitle()).isEqualTo("WEB-ТЕХНОЛОГИИ");
        assertThat(card.getNumberOfEmployees()).isEqualTo("9");
        assertThat(card.getLevel()).isEqualTo("47%");
    }

    @Test
    public void deleteSomeCompetency() {
        // given
        driver.get("http://tc-stage05.k8s.iba/");
        loginPage.login("zaitsev@tc.by", "welcome");
        driver.get("http://tc-stage05.k8s.iba/web/guest/strategy/org-potential");
        HrPage.deleteCompetency();
        Card card = HrPage.getOrgPotentialCardElementByNumber(7);

        assertThat(card.getTitle()).isEqualTo("ВЕДЕНИЕ СЧЕТОВ");

    }

/*
    @Test
    public void setLevelOfPriority() {
        // given
        String expectedTitle = "Высокий приоритет";
        driver.get("http://tc-stage01.k8s.iba/");
        loginPage.login("zaitsev@tc.by", "welcome");
        driver.get("http://tc-stage01.k8s.iba/web/guest/strategy/org-potential");
        // when
        HrPage.setLevel();
        // then
        assertThat(HrPage.levelOfPriority()).isEqualTo(expectedTitle);
    }
    */

    @Test
    public void setProblematicFilter() {
        // given
        //String firstProblem = "Большой разброс по уровням владения";
        //String secondProblem = "Большое количество сотрудников с низким уровнем владения";
        //String thirdProblem = "Малое количество сотрудников, владеющих компетенцией/ блоком компетенций";
        driver.get("http://tc-stage05.k8s.iba/");
        loginPage.login("zaitsev@tc.by", "welcome");
        driver.get("http://tc-stage05.k8s.iba/web/guest/strategy/org-potential");
        HrPage.setFilter();
        // then
        assertThat(HrPage.checkFilter()).isEqualTo(true);
    }

    @Test
    public void openJobProfile() {
        // given
        driver.get("http://tc-stage05.k8s.iba/");
        loginPage.login("zaitsev@tc.by", "welcome");
        driver.get("http://tc-stage05.k8s.iba/c/portal/layout?p_l_id=20833&p_p_id=TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet&p_p_lifecycle=0&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_action=getPositionProfilePage&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_positionId=233&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_specId=265&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_isAdd=false&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_reserve=false");
        // when
        String actualTitle = driver.getTitle();
        // then
        assertThat(actualTitle).isEqualTo("Профиль должности - Конструктор Талантов");
    }



    @Test
    public void exampleFileUploadRobot() throws Exception {
        String filepath = "C:\\Code\\Java\\Courses\\SeleniumEdu\\src\\test\\resources\\sikulitest\\somefile.txt";
        driver.get("http://tc-stage05.k8s.iba/");
        loginPage.login("zaitsev@tc.by", "welcome");
        driver.get("http://tc-stage05.k8s.iba/c/portal/layout?p_l_id=20833&p_p_id=TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet&p_p_lifecycle=0&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_action=getPositionProfilePage&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_positionId=233&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_specId=265&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_isAdd=false&_TCPositionProfileCatalog_WAR_TCPositionProfileCatalogportlet_reserve=false");
        Jobprofile.addFile();
        Jobprofile.setClipboardData(filepath);
        Jobprofile.robotActions();
    }


    @AfterEach
    public void tearDown() {
        driver.close();
    }

}
