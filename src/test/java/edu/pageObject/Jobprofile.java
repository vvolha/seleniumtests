package edu.pageObject;

import edu.wrapper.Button;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class Jobprofile {
    public static final Button EDIT = new Button(By.xpath(("//button[contains(@class, 'editSpecializationButton')]")));
    public static final Button ADD = new Button(By.id("add-attachment"));


    private WebDriver driver;

    public Jobprofile(WebDriver driver) {
        this.driver = driver;

    }

    public void clickEdit() {
        EDIT.click();
    }

    public void clickAdd() {
        ADD.click();
    }

    public void addFile() {
        clickEdit();
        clickAdd();

    }
    public void setClipboardData(String string) {
        //StringSelection is a class that can be used for copy and paste operations.
        StringSelection stringSelection = new StringSelection(string);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
    }

    public void robotActions() throws Exception {

        Robot robot = new Robot();

        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        Thread.sleep(1000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        Thread.sleep(5000);
    }

}
