package edu.pageObject;

import edu.wrapper.Button;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    public static final By LOGIN_INPUT = By.name("_58_login");
    public static final By PASSWORD_INPUT = By.name("_58_password");
    public static final Button ENTER_BUTTON = new Button(By.xpath("//button[@type='submit']"));
    public static final By ERROR_MESSAGE = By.xpath("//div[@class='login-error']/div");

    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void typeLogin(String login) {
        driver.findElement(LOGIN_INPUT).sendKeys(login);
    }

    public void typePassword(String password) {
        driver.findElement(PASSWORD_INPUT).sendKeys(password);
    }

    public void clickSubmit() {
        ENTER_BUTTON.click();
    }

    public void login(String login, String password) {
        typeLogin(login);
        typePassword(password);
        clickSubmit();
    }

    public String errorMessage() {
        return driver.findElement(ERROR_MESSAGE).getText();
    }
}
