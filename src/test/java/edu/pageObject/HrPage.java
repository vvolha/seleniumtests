package edu.pageObject;

import edu.wrapper.Button;
import edu.wrapper.Card;
import edu.wrapper.Checkbox;
import edu.wrapper.ErrorMessage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HrPage {
    public static final Button ADD_COMPETENCY =new Button(By.xpath("//button[@class='tc-btn tc-btn-secondary-primary undefined']"));
    public static final Checkbox  CHECK_BOX =new Checkbox(By.xpath("//li[@class='department department3']"));
    public static final Button ADD_BUTTON = new Button(By.xpath("//button[@class='ant-btn tc-btn tc-btn-primary ant-btn-primary']"));
    public static final Button DELETE_BUTTON =new Button(By.xpath("//div[@class='trash']"));
    public static final Card CURRENT_LEVEL = new Card(By.xpath("//div[@class='set-pointer block']//div[@class='block-title font-normal-12 black-card']"));
    public static final Checkbox FILTER= new Checkbox(By.xpath("//div[@class='tc-orgpotential_filter-container']"));

    private static final String ORG_POTENTIAL_CARD = "//div[contains(@class, 'tc-orgpotential_wrapper-card ')]";
    private static final Card TITLE = new Card (By.xpath(".//div[contains(@class, 'title')]"));
    private WebDriver driver;

    public HrPage(WebDriver driver) {
        this.driver = driver;

    }

    public Card getOrgPotentialCardElementByNumber(Integer number) {
        return new Card(By.xpath(ORG_POTENTIAL_CARD + "[" + number + "]"));
    }
    public void addCompetency(String competency) {
        ADD_COMPETENCY.click();
       WebElement searchField = driver.findElement(By.xpath("//input[@class='ant-input']"));
        searchField.sendKeys(competency);
        searchField.sendKeys(Keys.ENTER);


    }
    public void pushCheckbox() {
        CHECK_BOX.check();

    }
    public void pushButtonAdd() {
        ADD_BUTTON.click();
    }

    public void deleteCompetency() {

        DELETE_BUTTON.click();
    }


    public boolean checkFilter() {
       return FILTER.isSelected();
    }

    public void setFilter() {
        FILTER.check();
        checkFilter();
    }



}
